from myhdl import enum, Signal, block, always_seq, intbv, instances


@block
def sequence_search(data_in, data_out, clk, reset, seq_to_find=()):
    state = enum('READ_DATA', 'WRITE_COUNT')
    counter = Signal(intbv(0)[32:])
    actual_state = Signal(state.READ_DATA)
    sequence_ok_char_counter = Signal(intbv(0)[32:])

    assert len(seq_to_find) > 0
    seq_to_find_sig = [Signal(intbv(val)[32:]) for val in seq_to_find]

    for i in range(len(seq_to_find_sig)):
        seq_to_find_sig[i].driven = True

    @always_seq(clk.posedge, reset=reset)
    def logic():
        
        if actual_state == state.READ_DATA:
            data_out.tvalid.next = 0
            data_out.tlast.next = 0
            data_in.tready.next = 1
            if data_in.tvalid == 1:

                if data_in.tdata == seq_to_find_sig[sequence_ok_char_counter]:
                    sequence_ok_char_counter.next = sequence_ok_char_counter + 1
                else:
                    sequence_ok_char_counter.next = 0

                if sequence_ok_char_counter.next == len(seq_to_find):
                    counter.next = counter + 1
                    sequence_ok_char_counter.next = 0

                if data_in.tlast == 1:
                    actual_state.next = state.WRITE_COUNT

        elif actual_state == state.WRITE_COUNT:
            data_in.tready.next = 0
            data_out.tvalid.next = 1
            data_out.tlast.next = 1
            data_out.tdata.next = counter
            if data_out.tready == 1:
                counter.next = 0
                actual_state.next = state.READ_DATA

        else:
            raise ValueError("Undefined state")

    return instances()
