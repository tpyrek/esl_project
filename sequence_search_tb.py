from myhdl import *
import os
from random import randint
from sequence_search import sequence_search
from axis import Axis
from clk_stim import clk_stim


@block
def testbench(vhdl_output_path = None):

    reset = ResetSignal(0, active=0, async=False)
    clk = Signal(bool(0))

    data_in = Axis(32)
    data_out = Axis(32)

    clk_gen = clk_stim(clk, period=10)

    seq_to_find = (2, 3, 4, 5, 6)

    @instance
    def reset_gen():
        reset.next = 0
        yield delay(54)
        yield clk.negedge
        reset.next = 1

    @instance
    def write_stim():
        values = [a for i in range(5) for a in range(10)]
        i = 0
        yield reset.posedge
        while i < len(values):
            yield clk.negedge
            data_in.tvalid.next = 1
            data_in.tdata.next = values[i]
            if i == len(values) - 1:
                data_in.tlast.next = 1
            else:
                data_in.tlast.next = 0
            if data_in.tready == 1:
                i += 1
        yield clk.negedge
        data_in.tvalid.next = 0

    @instance
    def read_stim():
        yield reset.posedge
        yield delay(61)
        yield clk.negedge
        data_out.tready.next = 1
        while True:
            yield clk.negedge
            if data_out.tlast == 1:
                break

        for i in range(10):
            yield clk.negedge
        raise StopSimulation()

    sequence_search_instance = sequence_search(data_in=data_in, data_out=data_out, clk=clk, reset=reset, seq_to_find=seq_to_find)

    if vhdl_output_path is not None:
        sequence_search_instance.convert(hdl='VHDL', path=vhdl_output_path, initial_values=True)

    return instances()


if __name__ == '__main__':
    trace_save_path = 'out/testbench/'
    vhdl_output_path = 'out/vhdl/'
    os.makedirs(os.path.dirname(trace_save_path), exist_ok=True)
    os.makedirs(os.path.dirname(vhdl_output_path), exist_ok=True)

    for the_file in os.listdir(trace_save_path):
        file_path = os.path.join(trace_save_path, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
        except Exception as e:
            print(e)

    tb = testbench(vhdl_output_path)
    tb.config_sim(trace=True, directory=trace_save_path, name='sequence_search_tb')
    tb.run_sim()
